FROM gcr.io/kaniko-project/executor:debug

ENV SSL_CERT_DIR=/certs

SHELL ["/busybox/sh", "-c"]